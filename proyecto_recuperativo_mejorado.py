#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class MainWindow():

    # metodo constructor
    def __init__(self):
        
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/window.ui")
        
        #ventana principal
        self.window = self.builder.get_object("principal")
        self.window.maximize()
        self.window.set_title("Proyecto")
        self.window.connect("destroy", Gtk.main_quit)

        self.suma = 0

        self.window.show_all()
        # cuadro de texto 1
        self.texto1 = self.builder.get_object("entry_text1")

        # cuadro de texto 2
        self.texto2 = self.builder.get_object("entry_text2")

        # boton aceptar
        self.boton_aceptar = self.builder.get_object("btn_aceptar")

        # boton reset
        self.boton_reiniciar = self.builder.get_object("btn_reiniciar")

        # datos
        self.texto1.connect("activate", self.suma_chars)
        self.texto2.connect("activate", self.suma_chars)


        self.boton_aceptar.connect("clicked", self.on_btn_aceptar_click)
        self.boton_reiniciar.connect("clicked", self.on_btn_reiniciar_click)

    def suma_chars(self, btn=None):

        self.leer1 = self.texto1.get_text()
        self.leer2 = self.texto2.get_text()

        try:
            numero1 = int(self.leer1)
            numero2 = int(self.leer2)

        except ValueError:
            numero1=0
            numero2=0
            for letra in self.leer1:
                if letra.lower() in "aeiou":
                    numero1=numero1+1
            
            for letra in self.leer2:
                if letra.lower() in "aeiou":
                    numero2=numero2+1
        self.suma = numero1 + numero2




    def on_btn_aceptar_click(self, btn=None):

        self.suma_chars()
        self.result_dialog = result_dialog(self.leer1, self.leer2, self.suma)

    def on_btn_reiniciar_click(self, btn=None):

        self.warning = warning_dialog()
        response = self.warning.window.run()

        if response == Gtk.ResponseType.OK:
            self.texto1.set_text("")
            self.texto2.set_text("")
            self.suma = 0


class result_dialog():

    def __init__(self, text1, text2, suma):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("guia1_UI.glade")

        self.window = self.builder.get_object("result_window")
        self.accept = self.builder.get_object("info_accept")

        self.accept.connect("clicked", self.close_window)
        self.window.format_secondary_text("Se ha ingresado los textos "
                                          "%s y %s \n"
                                          "Cuyos valores suman: %d" %(text1,
                                                                      text2,
                                                                      suma))
                                                                      
        self.window.show_all()

    def close_window(self, btn=None):
        self.window.destroy()


class warning_dialog():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("guia1_UI.glade")

        self.window = self.builder.get_object("warning_window")
        self.cancel = self.builder.get_object("warning_cancel")
        self.accept = self.builder.get_object("warning_accept")

        self.window.set_markup("Está seguro que desea borrar los entries?")
        self.window.show_all()

        self.cancel.connect("clicked", self.cancel_close)
        self.accept.connect("clicked", self.accept_close)

    def cancel_close(self, btn=None):
        self.window.destroy()

    def accept_close(self, btn=None):
        self.window.destroy()


if __name__ == "__main__":

    MainWindow()
    Gtk.main()
